<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ProductController@home')->name('home');
Route::post('/store_product','ProductController@store_product')->name('store_product');
Route::put('/update_product/{id}','ProductController@update_product')->name('update_product');
Route::get('/update_status', 'ProductController@update_status')->name('update_status');
Route::get('/imagedetail/{id}','ProductController@imagedetail')->name('imagedetail');
Route::post('/update_image/{id}','ProductController@update_image')->name('update_image');
