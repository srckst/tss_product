$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function(){
    $('#formProduct').on('submit',function(e){
        e.preventDefault();

        var form_data = new FormData(this);
        var file = new FormData(this);
        let TotalFiles = $('#productImage')[0].files.length; //Total files
        let files = $('#productImage')[0];
        for (let i = 0; i < TotalFiles; i++) {
            file.append('pi_image' + i, files.files[i]);
        }
        file.append('TotalFiles', TotalFiles);

        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: '/store_product',
            processData: false,
            contentType: false,
            data: form_data , file,
            beforeSend: function(){
                swal({
                    title: "Please wait",
                    text: "Just a moment , data will storing to data base",
                    button: false
                });
            },
            success : function(){
                swal({
                    title: "Successful",
                    text: "Just a moment , page will refresh soon",
                    icon: "success",
                    button: false
                });
                window.setTimeout(function(){
                    location.reload();
                },1350);
            },
            error : function(){
                swal({
                    title: "Something wrong",
                    text: "Please check your input",
                    icon: "error",
                    button: true
                });
            }
        });
    });
});

$(document).ready(function(){
    $('#product-list').on('change','.pdStatus',function(){
        var status = $(this).prop('checked') == true ? 1 : 0;
        var id = $(this).data('statid');

        $.ajax({
            type: "GET",
            dataType: "json",
            url: "/update_status",
            data: {
                "pd_id": id,
                "pd_status": status
            },
            success: function(res){
                if (res.pd_status == 1) {
                    notif({
                        msg: "<b>Turn on product <u>"+res.pd_name+"</u></b>",
                        type: "success"
                    });
                }
                else {
                    notif({
                        msg: "<b>Turn off product <u>"+res.pd_name+"</u></b>",
                        type: "warning"
                    });
                }
            },
            error: function(){
                notif({
                    msg: "<b>Success:</b>Something wrong !",
                    type: "error"
                });
            }
        });
    });
});

$(document).ready(function(){
    $('#formEdit').on('submit',function(e){
        e.preventDefault();
        var id = $('#hiddenid').val();
        $.ajax({
            type: "PUT",
            url: "update_product/"+id,
            data: $('#formEdit').serialize(),
            beforeSend: function(){
                swal({
                    title: "Please wait",
                    text: "Just a moment , data will storing to data base",
                    button: false
                });
            },
            success: function(){
                swal({
                    title: "Successful",
                    text: "Just a moment , page will refresh soon",
                    icon: "success",
                    button: false
                });

                window.setTimeout(function(){
                    location.reload();
                },1350);

            },
            error : function(){
                swal({
                    title: "Something wrong",
                    text: "Please check your input",
                    icon: "error",
                    button: true
                });
            }
        });
    });
});

$(document).ready(function(){
    $('#formEditImage').on('submit',function(e){
        e.preventDefault();
        var id = $('#pdimgid').val();
        var file = new FormData(this);
        let TotalFiles = $('#editproductImage')[0].files.length; //Total files
        let files = $('#editproductImage')[0];
        for (let i = 0; i < TotalFiles; i++) {
            file.append('pi_image' + i, files.files[i]);
        }
        file.append('TotalFiles', TotalFiles);

        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: '/update_image/'+id,
            processData: false,
            contentType: false,
            data: file,
            beforeSend: function(){
                swal({
                    title: "Please wait",
                    text: "Just a moment , data will storing to data base",
                    button: false
                });
            },
            success : function(res){
                swal({
                    title: "Successful",
                    text: "Just a moment , page will refresh soon",
                    icon: "success",
                    button: false,
                    timer: 1350
                });
                $.get({
                    url:'imagedetail/'+res.id,
                    beforeSend: function(){
                        $('#showImage').html("<h3 class='text-center'>กำลังโหลดข้อมูล...</h3>");
                    },
                    success:function( data ) {

                        $('#showImage').html(data);
                    },
                    complete:function(data){
                    },
                });
            },
            error : function(){
                swal({
                    title: "Something wrong",
                    text: "Please check your input",
                    icon: "error",
                    button: true
                });
            }
        });
    });
});
