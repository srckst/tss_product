$(document).ready( function () {
    $('#product-list').DataTable();
} );

$(document).ready(function(){
    $('.editpdBtn').on('click',function(){
        var pd_id = $(this).data('pdid');
        var pd_price = $(this).data('pdprice');
        var pd_name = $(this).data('pdname');

        $('#editproductName').val(pd_name);
        $('#editproductPrice').val(pd_price);
        $('#hiddenid').val(pd_id);
    });
});

$(document).ready(function(){
    $('#productImage').on('change', function(){
        if ($(this).get(0).files.length < 1) {
            $('.nameimgfile').text('Choose image');
        }
        else if ($(this).get(0).files.length < 2) {
            $('.nameimgfile').text($(this).val());
        }
        else{
            $('.nameimgfile').text($(this).get(0).files.length+' Files selected');
        }
    });
});

$(document).ready(function(){
    $('.imagedetail').on('click',function(){
        var id = $(this).data('pdimgid');
        $('#pdimgid').val(id);
        $.get({
            url:'imagedetail/'+id,
            beforeSend: function(){
                $('#showImage').html("<h3 class='text-center'>กำลังโหลดข้อมูล...</h3>");
            },
            success:function( data ) {

                $('#showImage').html(data);
            },
            complete:function(data){
            },
        });
    });
});
