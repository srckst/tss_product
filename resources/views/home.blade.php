<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="{{ asset('RuangAdmin-master/img/logo/logo2.jpg') }}" rel="icon">
    <title>TSS Product - Home</title>
    <link href="{{ asset('RuangAdmin-master/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('RuangAdmin-master/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('RuangAdmin-master/css/ruang-admin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <link href="{{ asset('css/netliva_switch.css') }}" rel="stylesheet">
    <link href="{{ asset('css/notifIt.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
</head>

<body id="page-top">
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
                <div class="sidebar-brand-icon">
                    <img style="border-radius:20px;" src="{{ asset('RuangAdmin-master/img/logo/logo2.jpg') }}">
                </div>
                <div class="sidebar-brand-text mx-3">TSS Product</div>
            </a>
            <hr class="sidebar-divider my-0">
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="fas fa-fw fa-home"></i>
                    <span>Home</span></a>
                </li>
                <hr class="sidebar-divider">
            </ul>
            <!-- Sidebar -->
            <div id="content-wrapper" class="d-flex flex-column">
                <div id="content">
                    <!-- TopBar -->
                    <nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top">
                        <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>
                    </nav>
                    <!-- Topbar -->

                    <!-- Container Fluid-->
                    <div class="container-fluid" id="container-wrapper">
                        <div class="row mb-3">
                            <!-- Add new product button -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <a class="card-withoutline" href="#" data-toggle="modal" data-target="#addProduct">
                                    <div class="card h-100 card-hover">
                                        <div class="card-body">
                                            <div class="row align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-uppercase mb-1"></div>
                                                    <div class="h5 mb-0 font-weight-bold text-primary">Add Product</div>
                                                    <div class="mt-2 mb-0 text-muted text-xs">
                                                        <span>Add new product here</span>
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-plus fa-2x text-success"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-xl-12 col-lg-12 mb-4">
                                <div class="card">
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h5 class="m-0 font-weight-bold text-primary">Product List</h5>
                                    </div>
                                    <div class="container-fluid">
                                        <div class="table-responsive">
                                            <table id="product-list" class="table align-items-center table-flush">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th class="text-center">Product Name</th>
                                                        <th class="text-center">Price (Bath)</th>
                                                        <th class="text-center">Status</th>
                                                        <th class="text-center">Image</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($product_list as $product_list)
                                                        <tr>
                                                            <td class="text-center">{{ $loop->iteration }}</td>
                                                            <td class="text-center"><span class="badge badge-danger font15">{{ $product_list->pd_name }}</span></td>
                                                            <td class="text-center"><span class="badge font15 badge-warning">{{ number_format($product_list->pd_price,2) }} ฿</span></td>
                                                            <td class="text-center"><input type="checkbox" data-statid="{{ $product_list->pd_id }}" class="pdStatus" netliva-switch data-active-text="On" data-passive-text="Off" data-active-color="#40bf40" data-passive-color="#ff4d4d" {{ $product_list->pd_status ? 'checked' : '' }}></td>
                                                            <td class="text-center"><a href="#" data-pdimgid="{{ $product_list->pd_id }}" data-target="#editImage" data-toggle="modal" class="text-info font24 imagedetail"><i class="fa fa-image"></i> </a></td>
                                                            <td class="text-center"><a href="#" data-target="#editProduct" data-toggle="modal" data-pdid="{{ $product_list->pd_id }}" data-pdprice="{{ number_format($product_list->pd_price,2) }}" data-pdname="{{ $product_list->pd_name }}" class="btn btn-sm btn-primary editpdBtn">Edit</a></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card-footer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---Container Fluid-->
                </div>
                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>For assessment test only</b>
                            </span>
                        </div>
                    </div>
                </footer>
                <!-- Footer -->
            </div>
        </div>

        <!-- Scroll to top -->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <script src="{{ asset('RuangAdmin-master/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('RuangAdmin-master/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('RuangAdmin-master/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('RuangAdmin-master/js/ruang-admin.min.js') }}"></script>
        <script src="{{ asset('js/general.js') }}"></script>
        <script src="{{ asset('js/crud.js') }}"></script>
        <script src="{{ asset('js/netliva_switch.js') }}"></script>
        <script src="{{ asset('js/notifIt.min.js') }}"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </body>

    <!-- Add product modal -->
    <div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="addProductModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addProductModalLabel"><b>Add Product</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <form id="formProduct" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="productName">Product Name</label>
                                <input type="text" name="pd_name" class="form-control" id="productName" placeholder="Ex. Jean" required>
                            </div>
                            <div class="form-group">
                                <label for="productPrice">Price</label>
                                <input type="number" name="pd_price" class="form-control" id="productPrice" placeholder="Ex. 159.50" required>
                            </div>
                            <div class="form-group">
                                <label>Product Image <small>(Allow select mutiple images)</small> </label>
                                <div class="custom-file">
                                    <input style="cursor:pointer;" name="pi_image[]" type="file" class="custom-file-input addmorefileinput" id="productImage" accept="image/*" multiple required>
                                    <label for="productImage" class="custom-file-label nameimgfile">Choose image</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Remark</label>
                                <textarea class="form-control" name="pd_remark" rows="4" cols="80" placeholder="Ex. This is a limited product."></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Update produc detail modal -->
    <div class="modal fade" id="editProduct" tabindex="-1" role="dialog" aria-labelledby="updateProductModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="updateProductModalLabel"><b>Edit Detail</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <form id="formEdit" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input id="hiddenid" type="hidden" name="pd_id" value="">
                            <div class="form-group">
                                <label for="editproductName">Product Name</label>
                                <input type="text" name="pd_name" class="form-control" id="editproductName" placeholder="Ex. Jean">
                            </div>
                            <div class="form-group">
                                <label for="productPrice">Price</label>
                                <input type="number" name="pd_price" class="form-control" id="editproductPrice" placeholder="Ex. 159.50">
                            </div>
                            <div class="form-group">
                                <label>Remark</label>
                                <textarea id="editRemark" class="form-control" name="pd_remark" rows="4" cols="80" placeholder="Ex. This is a limited product."></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Save</button>
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- Product image detail -->
    <div class="modal fade" id="editImage" tabindex="-1" role="dialog" aria-labelledby="updateProductModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="updateProductModalLabel"><b>Product Image</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <form id="formEditImage" enctype="multipart/form-data">
                            <input id="pdimgid" type="hidden" name="pd_id" value="">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Product Image <small>(Allow select mutiple images)</small> </label>
                                <div class="custom-file">
                                    <input style="cursor:pointer;" name="pi_image[]" type="file" class="custom-file-input addmorefileinput" id="editproductImage" accept="image/*" multiple required>
                                    <label for="editproductImage" class="custom-file-label nameimgfile">Choose image</label>
                                </div>
                            </div>
                        </div>
                        <div id="showImage">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Save</button>
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</html>
