<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Import for use laravel query function
use DB;

//Import model
use App\Product;
use App\ProductImage;

class ProductController extends Controller
{
    public function home(){

        $product_list = Product::select('pd_id','pd_name','pd_price','pd_status')
        ->leftJoin('product_image','pd_id','=','pi_pd_id')
        ->groupBy('pd_id')
        ->get();

        $view = view('home',[
            'product_list'          =>      $product_list,
        ]);

        return $view;
    }

    public function store_product(Request $request){

        //Validate image
        $this->validate($request,[
            'pi_image.*' => 'mimes:jpeg,jpg,png,gif'
        ]);

        //Store product detail
        $store = new Product;
        $store->pd_name = $request->input('pd_name');
        $store->pd_price = $request->input('pd_price');
        $store->pd_status = 1;
        $store->pd_remark = $request->input('pd_remark');
        $store->save();

        // Stroe image
        $file = $request->file('pi_image');
        foreach ($file as $key => $files) {
            $img = new ProductImage;
            $img->pi_pd_id = $store->pd_id;
            $img->pi_image = $request->file('pi_image')[$key]->store('product_img','public'); // Storage image to public path
            $img->pi_remark = $request->input('pd_remark');
            $img->save();
        }
    }

    public function update_status(Request $request){
        //Update status
        $status = Product::find($request->pd_id);
        $status->pd_status = $request->pd_status;
        $status->save();

        return response()->json([
        'pd_status'    =>      $status->pd_status,
        'pd_name'      =>      $status->pd_name
        ]);
    }

    public function imagedetail($id){
        $img = ProductImage::select('pi_image')
        ->leftJoin('product','pi_pd_id','pd_id')
        ->where('pd_id',$id)
        ->get();

        echo "<div class='row'>";
        foreach ($img as $img) {
            $path = asset('storage/'.$img->pi_image);
            echo "<div class='col-md-6'>
                <img width='300' height='230' src='$path' alt=''>
            </div>";
        }
        echo "</div>";
    }

    public function update_product(Request $request ,$id){
        $update = Product::find($id);
        $update->pd_price = $request->input('pd_price');
        $update->pd_name = $request->input('pd_name');
        $update->pd_remark = $request->input('pd_remark');
        $update->save();
    }

    public function update_image(Request $request ,$id){

        //Validate image
        $this->validate($request,[
            'pi_image.*' => 'mimes:jpeg,jpg,png,gif'
        ]);

        // Stroe image
        $file = $request->file('pi_image');
        foreach ($file as $key => $files) {
            $img = new ProductImage;
            $img->pi_pd_id = $id;
            $img->pi_image = $request->file('pi_image')[$key]->store('product_img','public'); // Storage image to public path
            $img->pi_remark = $request->input('pd_remark');
            $img->save();
        }
        return response()->json([
            'id'    =>      $id
        ]);
    }
}
