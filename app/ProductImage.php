<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    //Set defualt table
    protected $table = "product_image";

    //Set defualt primary key
    protected $primaryKey = "pi_id";
}
