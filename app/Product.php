<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //Set defualt table
    protected $table = "product";

    //Set defualt primary key
    protected $primaryKey = "pd_id";
}
